package com.adallom.dedupstresstest;

import com.codahale.metrics.*;
import com.microsoft.azure.documentdb.*;
import com.microsoft.azure.documentdb.internal.Constants;
import org.apache.http.HttpStatus;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by idof on 21/05/2018.
 */

public class Main {
    private static final String HOST = "https://kustoesreplacement3.documents.azure.com:443/";
    private static final String MASTER_KEY = "tGptZH0p5C1d8Hy2SoMAyK6CPKHfAxQyVGEijQAGypGgriI9puEMxkKk1R2PRWT17zqmD0VwxbvQ9w0bN2iOkA==";
    private static final String COLLECTION_LINK = "/dbs/Activities/colls/EventIds";
    private static final int POOL_SIZE = 100;

    private static DocumentClient documentClient;

    private static final Timer creationStats = new Timer();
    private static final Meter chargesMeter = new Meter();
    private static final Histogram chargesHist = new Histogram(new ExponentiallyDecayingReservoir());


    private static ResourceResponse<Document> createSingleDocument(String id) throws DocumentClientException {
        Map<String, Object> document = new HashMap<>();
        document.put(Constants.Properties.ID, id);

        RequestOptions options = new RequestOptions();
        //options.setPartitionKey(new PartitionKey());

        ResourceResponse<Document> result;
        try (Timer.Context ignored = creationStats.time()) {
            result = documentClient.upsertDocument(COLLECTION_LINK, document, options, true);
        }

        long chargeFactored = (long) (result.getRequestCharge() * 1000);
        chargesMeter.mark(chargeFactored);
        chargesHist.update(chargeFactored);

        if (result.getStatusCode() != HttpStatus.SC_CREATED && result.getStatusCode() != HttpStatus.SC_OK) {
            System.err.println("Bad HTTP status!");
        }

        return result;
    }

    private static void createDocument(String id, boolean createDuplicate) {
        ResourceResponse<Document> response;
        try {
            response = createSingleDocument(id);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (response.getStatusCode() != HttpStatus.SC_CREATED) {
            System.err.println("Document not created even though it is not duplicated!");
        }

        if (createDuplicate) {
            try {
                response = createSingleDocument(id);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            if (response.getStatusCode() != HttpStatus.SC_OK) {
                System.err.println("Document created even though it is duplicated!");
            }
        }
    }

    private static String generateEventUid() {
        UUID uuid = UUID.randomUUID();
        String s = uuid.toString();
        return s + "---" + s;
    }

    private static void createWork() {
        LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(1000);
        ExecutorService executorService = new ThreadPoolExecutor(
                POOL_SIZE,
                POOL_SIZE,
                0L,
                TimeUnit.MILLISECONDS,
                workQueue,
                new ThreadPoolExecutor.CallerRunsPolicy());

        //noinspection InfiniteLoopStatement
        while (true) {
            String id = generateEventUid();
            boolean createDuplicate = ThreadLocalRandom.current().nextInt(10) == 0;
            executorService.execute(() -> createDocument(id, createDuplicate));
        }
    }

    private static void printStats() throws DocumentClientException {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.setPopulatePartitionKeyRangeStatistics(true);
        ResourceResponse<DocumentCollection> resourceResponse = documentClient.readCollection(COLLECTION_LINK, requestOptions);
        Collection<PartitionKeyRangeStatistics> partitionStatistics = resourceResponse.getResource().getCollectionPartitionStatistics();

        long documentsCount = partitionStatistics.stream().mapToLong(PartitionKeyRangeStatistics::getDocumentCount).sum();
        long totalSizeKB = partitionStatistics.stream().mapToLong(PartitionKeyRangeStatistics::getSizeInKB).sum();
        System.out.println(String.format("Documents count: %d. Total size: %d GB", documentsCount, totalSizeKB / 1024 / 1024));
    }

    public static void main(String[] args) throws Exception {
        ConnectionPolicy connectionPolicy = new ConnectionPolicy();
        //connectionPolicy.setConnectionMode(ConnectionMode.DirectHttps);
        RetryOptions retryOptions = new RetryOptions();
        retryOptions.setMaxRetryAttemptsOnThrottledRequests(19);
        retryOptions.setMaxRetryWaitTimeInSeconds(60);
        connectionPolicy.setRetryOptions(retryOptions);

        documentClient = new DocumentClient(
                HOST,
                MASTER_KEY,
                connectionPolicy,
                ConsistencyLevel.Strong);

        printStats();

        Thread dispatcherThread = new Thread(Main::createWork);
        dispatcherThread.setDaemon(true);
        dispatcherThread.start();

        long prevCount = 0;

        //noinspection InfiniteLoopStatement
        while (true) {
            int sleepTimeSeconds = 30;
            TimeUnit.SECONDS.sleep(sleepTimeSeconds);

            long currentCount = creationStats.getCount();
            double rate = creationStats.getOneMinuteRate();
            double meanActionTimeNanos = creationStats.getSnapshot().getMean();

            double chargePerSecond = chargesMeter.getOneMinuteRate() / 1000;
            Snapshot snapshot = chargesHist.getSnapshot();
            double avgCharge = snapshot.getMean() / 1000;
            System.out.println(String.format("Elapsed: %d sec. Created %d documents (%s per second). Avg time: %d ms. RU/s: %s (%s per action)",
                    sleepTimeSeconds,
                    currentCount - prevCount,
                    rate,
                    TimeUnit.NANOSECONDS.toMillis((long) meanActionTimeNanos),
                    chargePerSecond,
                    avgCharge
            ));
            prevCount = currentCount;
        }
    }
}
